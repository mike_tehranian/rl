The libraries needed to run my code are shown below:
* Python 3.4
* OpenAI Gym
* Sci-kit learn
* Matplotlib
* Numpy

I had issues installing Box2D using the steps in the instructions. So I installed Box2D using the following steps:

* $ conda create -n py34 python=3.4
* $ source activate py34
* $ conda install -c https://conda.anaconda.org/kne pybox2d
* $ conda install libgfortran==1

How To Run My Code (Python 3):
$ python lunar_lander_agent.py


How To Plot My Graphs (Python 3):
$ python plot_lunar_lander_agent.py

Files Generated:
* train_reward.png
* test_reward.png
