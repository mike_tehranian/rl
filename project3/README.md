The libraries used to run my code are shown below:
* Python 3.6
* CVXOPT 1.2.1
* Numpy 1.15.2
* Matplotlib

I installed CVXOPT using the following steps:
* $ conda install -c conda-forge cvxopt

How To Run My Code (Python 3):
$ python markov_games.py

Files Generated:
* q-learning.npy
* friend-q.npy
* foe-q.npy
* correlated-q.npy

How To Plot My Graphs (Python 3):
$ python plot_q_values.py

Files Generated:
* q-learning.png
* friend-q.png
* foe-q.png
* correlated-q.png
