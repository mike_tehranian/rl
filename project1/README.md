My code is based on the popular Sci-kit Learn libraries, also known
  as the SciPy software stack.


The libraries needed to run my code are shown below:
* Python 3.6.5
* Sci-kit learn
* Matplotlib
* Numpy

All of these packages can be installed via PIP (Python Package Index)
Instructions taken from: https://www.scipy.org/install.html
  $ python -m pip install --upgrade pip
  $ pip install --user numpy scipy matplotlib

How To Run My Code (Python 3):
$ python td_lambda.py

Files Generated:
* figure_3.png
* figure_4.png
* figure_5.png
